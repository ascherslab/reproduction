#!/bin/bash
# Partition for the job:
#SBATCH --partition=biosig

# Multithreaded (SMP) job: must run on one node and the cloud partition
#SBATCH --nodes=1

# The name of the job:
#SBATCH --job-name="vgg16 training"

# Maximum number of tasks/CPU cores used by the job:
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32

# The amount of memory in megabytes per process in the job:
#SBATCH --mem=4096

# The maximum running time of the job in days-hours:mins:sec
#SBATCH --time=0-0:20:00

# check that the script is launched with sbatch
if [ "x$SLURM_JOB_ID" == "x" ]; then
   echo "You need to submit your job to the queuing system with sbatch"
   exit 1
fi

# Run the job from the directory where it was launched (default)

# The job command(s):
/projects/biosig/users/ptrehan/mini/envs/vision/bin/python3 /projects/biosig/users/ptrehan/reproduction/train.py  --rundir output --backbone vgg16 --task acl --plane sagittal --epochs=50
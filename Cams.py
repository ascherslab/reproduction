import sys
import os
import time
sys.path.append('../../mrnet')

import numpy as np
import shutil
import cv2
from PIL import Image
import pdb
import matplotlib.pyplot as plt

import torch
from torch.nn import functional as F
from torchvision import models, transforms

from model import MRNet 
from loader import load_data
from tqdm.notebook import tqdm

INPUT_DIM = 224
MAX_PIXEL_VAL = 255
MEAN = 58.09
STDDEV = 49.73

class HookFeatures():
    features=None
    def __init__(self, m): 
        self.hook = m.register_forward_hook(self.hook_fn)
    def hook_fn(self, module, input, output): 
        self.features = ((output.cpu()).data).numpy()
    def remove(self): 
        self.hook.remove()
        
        
def CreatePatientDictionary(task, plane):
    _, valid_loader = load_data(task=task, plane=plane)
    patients = []
    for i, (image, label) in tqdm(enumerate(valid_loader), total=len(valid_loader)):
        patient_data = {}
        patient_data['mri'] = image
        patient_data['label'] = label[0][0].item()
        patient_data['id'] = '0' * (4 - len(str(i))) + str(i)
        patients.append(patient_data)
    return patients

def CreatePaths(dire, plane):
    paths = []
    for filename in os.listdir(os.path.join(dire, plane)):
        if filename.endswith(".npy"):
            paths.append(filename)
    return paths

def ExtractImageVol(filename,plane):
    dire = "data/valid"
    vol = np.load(os.path.join(dire, plane, filename))
    vol = np.stack((vol,)*3, axis=1)
    vol_tensor = torch.FloatTensor(vol)
    return vol_tensor

def LoadMRNetModel(task, plane):  
    model_name = [name  for name in os.listdir('models/vgg19') 
                  if (task in name) and 
                  (plane in name)][0]
    mrnet = MRNet(backbone='vgg19')
    mrnet.load_state_dict(torch.load(f'models/vgg19/{model_name}'))
    return mrnet

def returnCAM(activated_features, weight_softmax):
    features = activated_features.features
    size_upsample = (256,256)
    bz, nc, h, w = features.shape
    slice_cams = []
    for s in range(bz):
            cam =np.dot(weight_softmax,features[s].reshape((nc, h*w)))
            cam = cam.reshape(h, w)
            cam = cam - np.min(cam)
            cam_img = cam / np.max(cam)
            cam_img = np.uint8(255 * cam_img)
            slice_cams.append(cv2.resize(cam_img, size_upsample))
    return slice_cams

def CreateSlicesCams(case, plane, model, activated_features):
    patient_id = case['id']
    mri = case['mri']

    folder_path = f'./CAMS/{plane}/{patient_id}/'
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)
    os.makedirs(folder_path + 'slices/')
    os.makedirs(folder_path + 'cams/')
    num_slices = mri.shape[1]
    
    params = list(model.parameters())
    weight_softmax = np.squeeze(params[-2].cpu().data.numpy())
    
    num_slices = mri.shape[1]
    logit = model(mri.cuda())
    
    h_x = F.softmax(logit, dim=1).data.squeeze(0)
    probs, _ = h_x.sort(0, True)
    probs = probs.cpu().numpy()
    activated_features.remove()
    slice_cams = returnCAM(activated_features, weight_softmax)
    return slice_cams, num_slices, folder_path

def CreateCamOverlay(case, case_index, task, plane):
    mrnet = LoadMRNetModel(task=task, plane=plane)
    _ = mrnet.eval()
    mrnet.cuda()
    activated_features = HookFeatures(mrnet.model._modules.get("features"))
    slice_cams, num_slices, folder_path = CreateSlicesCams(case, plane, mrnet, activated_features)
    paths = CreatePaths(dire = "data/valid", plane=plane)
    vol_tensor = ExtractImageVol(paths[case_index],plane)
    case_cams = []
    case_slices = []
    for s in tqdm(range(num_slices), leave=False):
        slice_pil = (transforms.ToPILImage()(vol_tensor[s].cpu() / 255))
        slice_pil.save(folder_path + f'slices/{s}.png', dpi=(300, 300))
         
        img = vol_tensor[s].cpu().numpy()
        img = img.transpose(1, 2, 0)
        heatmap = (cv2.cvtColor(cv2.applyColorMap(
                    cv2.resize(slice_cams[s], (256, 256)),
                    cv2.COLORMAP_JET), 
                                cv2.COLOR_BGR2RGB)
                  )
        result = heatmap * 0.3 + img * 0.5  
        pil_img_cam = Image.fromarray(np.uint8(result))
        case_cams.append(pil_img_cam)
        case_slices.append(slice_pil)
        pil_img_cam.save(folder_path + f'cams/{s}.png', dpi=(300, 300))
    return case_slices, case_cams
#!/bin/bash
# output_dir=./output/output$(date +'%m-%d-%Y-%R').txt
# touch $output_dir
echo "------------------------- MRNet -------------------------" >> output/output.txt

epochs=50
backbone=vgg19
counter=1
lr=1e-06
tasks=(meniscus)
planes=(sagittal axial coronal)

for task in "${tasks[@]}"; do
    for plane in "${planes[@]}"; do
        com="python train.py  --rundir output --backbone $backbone --task $task --plane $plane --learning_rate=$lr --epochs=$epochs --gpu"
        echo "------------------------- Training in progress ($counter out of 3) ----------------------" >> output/output.txt
        echo "Running command $com" >> output/output.txt
        eval $com

        echo "Training done for $task-$plane with $epochs epochs and model is saved " >> output/output.txt
        
        echo "------------------------- Training done ($counter model) ----------------------------------" >> output/output.txt
        let "counter++"
    done
done

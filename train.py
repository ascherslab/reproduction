import os
import time
import argparse
import json
from datetime import datetime
from pathlib import Path

from sklearn import metrics
import numpy as np
import torch
from torchvision import transforms
from torchsample.transforms import RandomRotate, RandomTranslate, RandomFlip, ToTensor, Compose, RandomAffine


from evaluate import run_model
from loader import load_data
from model import MRNet


def train(rundir, backbone, task, plane, epochs, learning_rate, use_gpu):

    transformations = Compose([
        transforms.Lambda(lambda x: torch.Tensor(x)),
        RandomRotate(25),
        RandomTranslate([0.10, 0.10]),
        RandomFlip(),
    ])

    train_loader, valid_loader = load_data(
        task=task, plane=plane, transform=None, use_gpu=use_gpu)

    model = MRNet(backbone=backbone)

    if use_gpu:
        model = model.cuda()

    optimizer = torch.optim.Adam(
        model.parameters(), learning_rate, weight_decay=.01)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, patience=5, factor=.3, threshold=1e-4)

    best_val_loss = float('inf')

    for epoch in range(epochs):

        t_start = time.time()
        train_loss, train_auc, _, _ = run_model(
            model=model,
            loader=train_loader,
            epoch=epoch,
            num_epochs=epochs,
            train=True,
            optimizer=optimizer)
        val_loss, val_auc, _, _ = run_model(
            model=model,
            loader=valid_loader,
            epoch=epoch,
            num_epochs=epochs)
        scheduler.step(val_loss)
        t_end = time.time()

        delta = t_end - t_start
        print("train loss : {0} | train auc {1} | val loss {2} | val auc {3} | elapsed time {4} s".format(
            train_loss, train_auc, val_loss, val_auc, delta), file=open("output/output.txt", "a"))

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            file_name = f'model_{task}_{plane}_val_auc_{val_auc:0.4f}_train_auc_{train_auc:0.4f}_epoch_{epoch+1}.pth'
            for f in os.listdir(f'./models/{backbone}/'):
                if (args.task in f) and (args.plane in f):
                    os.remove(f'./models/{backbone}/{f}')
            torch.save(model.state_dict(), f'./models/{backbone}/{file_name}')


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--rundir', type=str, required=True)
    parser.add_argument('-t', '--task', type=str, required=True,
                        choices=['abnormal', 'acl', 'meniscus'])
    parser.add_argument('-p', '--plane', type=str, required=True,
                        choices=['sagittal', 'coronal', 'axial'])
    parser.add_argument('-n', '--backbone', type=str, required=True,
                        choices=[
                            'Inception_v3',
                            'alexnet-removed-slices',
                            'resnet18', 'resnet50', 
                            'alexnet', 'vgg16',
                            'vgg19', 'squeezenet1_0', 
                            'GoogLeNet','DenseNet'])
    parser.add_argument('--seed', default=42, type=int)
    parser.add_argument('--gpu', action='store_true')
    parser.add_argument('--learning_rate', default=1e-05, type=float)
    parser.add_argument('--weight_decay', default=0.01, type=float)
    parser.add_argument('--epochs', default=50, type=int)
    parser.add_argument('--max_patience', default=5, type=int)
    parser.add_argument('--factor', default=0.3, type=float)
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()

    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if args.gpu:
        torch.cuda.manual_seed_all(args.seed)

    os.makedirs(args.rundir, exist_ok=True)

    with open(Path(args.rundir) / 'args.json', 'w') as out:
        json.dump(vars(args), out, indent=4)

    train(args.rundir, args.backbone, args.task, args.plane,
          args.epochs, args.learning_rate, args.gpu)

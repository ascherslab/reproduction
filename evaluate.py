import argparse
import matplotlib.pyplot as plt
import os
import numpy as np
import torch

from sklearn import metrics
from torch.autograd import Variable

from loader import load_data
from model import MRNet


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, required=True)
    parser.add_argument('--split', type=str, required=True)
    parser.add_argument('--diagnosis', type=int, required=True)
    parser.add_argument('--gpu', action='store_true')
    return parser


def run_model(model, loader, epoch, num_epochs, train=False, optimizer=None, log_every=100):
    preds = []
    labels = []

    if train:
        model.train()
    else:
        model.eval()

    total_loss = 0.
    num_batches = 0
    for batch in loader:
        if train:
            optimizer.zero_grad()

        vol, label = batch
        if loader.dataset.use_gpu:
            vol = vol.cuda()
            label = label.cuda()
        vol = Variable(vol)
        label = Variable(label)

        logit = model.forward(vol)
        loss = loader.dataset.weighted_loss(logit, label)
        total_loss += loss.item()

        pred = torch.sigmoid(logit)
        pred_npy = pred.data.cpu().numpy()[0][0]
        label_npy = label.data.cpu().numpy()[0][0]

        preds.append(pred_npy)
        labels.append(label_npy)

        if (num_batches % log_every == 0) & (num_batches > 0):
            print('''[Epoch: {0} / {1} |Single batch number : {2} / {3} ]|'''.
                  format(
                      epoch + 1,
                      num_epochs,
                      num_batches,
                      len(loader)
                  ), file=open("output/output.txt", "a"))
        if train:
            loss.backward()
            optimizer.step()
        num_batches += 1

    avg_loss = total_loss / num_batches
    # precision, recall, thresholds = metrics.precision_recall_curve(labels,preds)
    # fscore = (2 * precision * recall) / (precision + recall)
    # ix = np.argmax(fscore)
    fpr, tpr, _ = metrics.roc_curve(labels, preds)
    auc = metrics.auc(fpr, tpr)

    # print(labels,preds)
    # mcc = metrics.matthews_corrcoef(y_true=labels, y_pred=preds)

    return avg_loss, auc, preds, labels


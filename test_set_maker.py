# mkdir data/test
# mkdir data/test/axial
# mkdir data/test/coronal
# mkdir data/test/sagittal

import os
import random
import pandas as pd
import numpy as np
import shutil
import csv

abnormal_label_dict ={}
acl_label_dict ={}
meniscus_label_dict={}
test_filenames = []

for i, line in enumerate(open('test.csv').readlines()):
        line = line.strip().split(',')
        filename = line[3]
        test_filenames.append(filename)
        abnormal_label = line[0]
        acl_label = line[1]
        meniscus_label = line[2]
        abnormal_label_dict[filename] = int(abnormal_label)
        acl_label_dict[filename] = int(acl_label)
        meniscus_label_dict[filename] = int(meniscus_label)

test_df = pd.DataFrame()
test_abnormal_pd = pd.Series(abnormal_label_dict)
test_acl_pd = pd.Series(acl_label_dict)
test_meniscus_pd = pd.Series(meniscus_label_dict)

test_df = pd.concat((
    test_abnormal_pd.rename('abnormal'),
    test_acl_pd.rename('acl'),
    test_meniscus_pd.rename('meniscus')),
    axis=1)

test_abnormal_pd.to_csv('data/test-abnormal.csv',header=False)
test_acl_pd.to_csv('data/test-acl.csv',header=False)
test_meniscus_pd.to_csv('data/test-meniscus.csv',header=False)

for plane in ['axial','sagittal','coronal']:
    for item in test_df.iterrows():
        shutil.move(f"data/train/{plane}/{item[0]}.npy", f"data/test/{plane}/{item[0]}.npy")
        
for task in ['abnormal','acl','meniscus']:
    f = open(f'data/train-updated-{task}.csv', 'w')
    with f:
        writer = csv.writer(f)
        for i, line in enumerate(open(f'data/train-{task}.csv').readlines()):
                line = line.strip().split(',')
                filename = line[0]
                label = line[1]
                if filename not in test_filenames:
                    writer.writerow(line)
import numpy as np
import os
import pickle
import torch
import torch.nn.functional as F
import torch.utils.data as data

from torch.autograd import Variable

INPUT_DIM = 224
MAX_PIXEL_VAL = 255
MEAN = 58.09
STDDEV = 49.73
MAX_SLICES = {'axial':61,'coronal':58,'sagittal':51}


class Dataset(data.Dataset):
    def __init__(self, datadir, task, plane, transform, use_gpu):
        super().__init__()
        self.use_gpu = use_gpu
        self.plane = plane
        self.task = task
        self.transform = transform

        label_dict = {}
        self.paths = []

        if datadir[-1] == "/":
            datadir = datadir[:-1]
        self.datadir = datadir

        for i, line in enumerate(open(datadir+'-'+task+'.csv').readlines()):
            line = line.strip().split(',')
            filename = line[0]
            label = line[1]
            label_dict[filename] = int(label)

        for filename in os.listdir(os.path.join(datadir, plane)):
            if filename.endswith(".npy"):
                self.paths.append(filename)

        self.labels = [label_dict[path.split(".")[0]] for path in self.paths]

        neg_weight = np.mean(self.labels)
        self.weights = [neg_weight, 1 - neg_weight]

    def weighted_loss(self, prediction, target):
        weights_npy = np.array([self.weights[int(t[0])] for t in target.data])
        weights_tensor = torch.FloatTensor(weights_npy)
        if self.use_gpu:
            weights_tensor = weights_tensor.cuda()
        loss = F.binary_cross_entropy_with_logits(
            prediction, target, weight=Variable(weights_tensor))
        return loss

    def __getitem__(self, index):
        filename = self.paths[index]
        vol = np.load(os.path.join(self.datadir, self.plane, filename))
        # if vol.shape[0]<MAX_SLICES[self.plane]:
        #     slice_pad = np.zeros([MAX_SLICES[self.plane]-vol.shape[0],256,256],dtype = int)
        #     vol = np.concatenate((vol, slice_pad), axis=0)  
        pad = int((vol.shape[2] - INPUT_DIM)/2)
        vol = vol[:,pad:-pad,pad:-pad]
            
        vol = (vol-np.min(vol))/(np.max(vol)-np.min(vol))*MAX_PIXEL_VAL
       
        vol = (vol - MEAN) / STDDEV
            
        if(self.transform != None):
            vol = self.transform(vol)
        # if (self.task != 'abnormal'):
        #     vol = vol[1:vol.shape[0]-1]
        
        vol = np.stack((vol,)*3, axis=1)
        vol_tensor = torch.FloatTensor(vol)

        label_tensor = torch.FloatTensor([self.labels[index]])


        return vol_tensor, label_tensor

    def __len__(self):
        return len(self.paths)


def load_data(task="acl", plane="sagittal", transform=None, use_gpu=False, batch_size=1):
    train_dir = "data/train"
    valid_dir = "data/valid"

    train_dataset = Dataset(train_dir, task, plane, transform, use_gpu)
    valid_dataset = Dataset(valid_dir, task, plane, transform, use_gpu)

    train_loader = data.DataLoader(
        train_dataset, batch_size=batch_size, num_workers=8, shuffle=True)
    valid_loader = data.DataLoader(
        valid_dataset, batch_size=batch_size, num_workers=8, shuffle=False)

    return train_loader, valid_loader

import torch
import torch.nn as nn

from torchvision import models

class MRNet(nn.Module):
    def __init__(self, backbone):
        super().__init__()
        self.backbone = backbone
        if self.backbone == 'alexnet' or self.backbone == 'alexnet-removed-slices':
            self.model = models.alexnet(pretrained=True)
            self.gap = nn.AdaptiveAvgPool2d(1)
            self.classifier = nn.Linear(256, 1)
        elif self.backbone == 'resnet18':
            resnet = models.resnet18(pretrained=True)
            modules = list(resnet.children())[:-1]
            self.model = nn.Sequential(*modules)
            self.gap = nn.AdaptiveAvgPool2d(1)
            self.classifier = nn.Linear(512,1)
        elif self.backbone == 'resnet50':
            resnet = models.resnet50(pretrained=True)
            modules = list(resnet.children())[:-1]
            self.model = nn.Sequential(*modules)
            self.gap = nn.AdaptiveAvgPool2d(1)
            self.classifier = nn.Linear(2048,1)
        elif self.backbone == 'vgg16':
            self.model = models.vgg16(pretrained=True)
            self.gap = nn.AdaptiveAvgPool2d(1)
            self.classifier = nn.Linear(512, 1)
        elif self.backbone == 'vgg19':
            self.model = models.vgg19(pretrained=True)
            self.gap = nn.AdaptiveAvgPool2d(1)
            self.classifier = nn.Linear(512, 1)
        elif self.backbone == 'squeezenet1_0':
            self.model = models.squeezenet1_0()
            self.gap = nn.AdaptiveAvgPool2d(1)
            self.classifier = nn.Linear(512, 1)
        elif self.backbone == 'GoogLeNet':
            self.model = models.GoogLeNet()
            self.gap = nn.AdaptiveAvgPool2d(1)
            self.classifier = nn.Linear(512, 1)

    def forward(self, x):
        if self.backbone == 'alexnet' or self.backbone == 'alexnet-removed-slices':
            x = torch.squeeze(x, dim=0) # only batch size 1 supported
            x = self.model.features(x)
            x = self.gap(x).view(x.size(0), -1)
            # x = nn.functional.pad(input=x, pad=(0, 0, 0, 50 - x.shape[0]), mode='constant', value=0)
            # x = torch.flatten(x)
            x = torch.max(x, 0, keepdim=True)[0]
            x = self.classifier(x)
            return x.reshape(1,1)
        elif self.backbone == 'alexnet-1':
            x = torch.squeeze(x, dim=0) # only batch size 1 supported
            x = self.model(x)
            x = self.gap(x).view(x.size(0), -1)
            x = torch.max(x, 0, keepdim=True)[0]
            x = self.classifier(x)
            return x
        elif self.backbone == 'resnet50':
            x = torch.squeeze(x, dim=0) # only batch size 1 supported
            x = self.model(x)
            x = self.gap(x).view(x.size(0), -1)
            x = torch.max(x, 0, keepdim=True)[0]
            x = self.classifier(x)
            return x
        elif self.backbone == 'vgg16' or self.backbone == 'vgg19':
            x = torch.squeeze(x, dim=0) # only batch size 1 supported
            x = self.model.features(x)
            x = self.gap(x).view(x.size(0), -1)
            x = torch.max(x, 0, keepdim=True)[0]
            x = self.classifier(x)
            return x
        elif self.backbone == 'squeezenet1_0':
            x = torch.squeeze(x, dim=0) # only batch size 1 supported
            x = self.model.features(x)
            x = self.gap(x).view(x.size(0), -1)
            x = torch.max(x, 0, keepdim=True)[0]
            x = self.classifier(x)
            return x
        elif self.backbone == 'GoogLeNet':
            x = torch.squeeze(x, dim=0) # only batch size 1 supported
            x = self.model(x)
            x = self.gap(x).view(x.size(0), -1)
            x = torch.max(x, 0, keepdim=True)[0]
            x = self.classifier(x)
            return x
        elif self.backbone == 'DenseNet':
            x = torch.squeeze(x, dim=0) # only batch size 1 supported
            x = self.model.features(x)
            x = self.gap(x).view(x.size(0), -1)
            x = torch.max(x, 0, keepdim=True)[0]
            x = self.classifier(x)
            return x